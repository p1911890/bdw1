<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/utilisateur.php';

$stateMsg = "";

/* si un formulaire a été envoyé, on encrypte le mot de passe et on vérifie la correspondance 
 * entre le couple pseudo-mot de passe du formulaire et ceux de la base de donnée */
if(isset($_POST["valider"])){
    $pseudo = $_POST["pseudo"];
    $hashMdp = md5($_POST["mdp"]);
    
    $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
    
    $exist = getUser($pseudo, $hashMdp, $link);
    /* si le couple pseudo-mdp correspond, on initialise la session au nom de l'utilisateur, et on revient à l'accueil*/
    if($exist){
        $_SESSION["logged"] = "true";
        $_SESSION["user"] = $pseudo;
        $_SESSION["ConTime"] = time();
        header('Location: index.php');
    /* si il n'existe pas, on se contente d'indiquer que l'utilisateur a fait une erreur, et rien d'autre n'est modifié*/
    }else{
        $stateMsg = "Le couple pseudo/mot de passe ne correspond &agrave; aucun utilisateur enregistr&eacute;";
    }
}
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Se connecter pour ajouter des images</title>
  <link rel="stylesheet" href="./css/style_connexion.css">
  <link rel="icon" href="favicon.ico" />
</head>
<body>
    <h2>Se connecter</h2>
    <div class="loginBanner">
    <div class="errorMsg"><?php echo $stateMsg; ?></div>
    <?php if(isset($successMsg)){echo $successMsg;} ?>
        <form action="connexion.php" method="POST">
            <table>
                <tr><td class="loginInfo">Pseudo:</td><td><input type="text" name="pseudo"></td></tr>
                <tr><td class="loginInfo">Mot de passe:</td><td><input type="password" name="mdp"></td></tr>
                <br/>
                <tr><td><input class="button" type="submit" name="valider" value="Se connecter">
            </table>
            </form>
            </br>
            </br>    
    <a class="loginInfo" href="index.php">Retour au menu</a>
</body>
</html>
