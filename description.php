<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/images.php';
$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);

//Cette fonctionne calcule le temps de connection sur le compte */
function transformation($temps){
	$heures = (int) ($temps / 3600);
	$temps = $temps % 3600;
	$minutes = (int) ($temps / 60);
	$temps = $temps % 60;
	$secondes = $temps;
	
	return $heures." heures ".$minutes." minutes ".$secondes." secondes."; 
	
	
}
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Description de la photo</title>
  <link rel="stylesheet" href="./css/style_description.css">
  <link rel="icon" href="favicon.ico" />
</head>
<body>
</br>
<a class="loginInfo" href="index.php">Retour au menu</a>
<h1> Les détails sur cette photo </h1>
<div class=photo>

<?php
//Ce if montre la description de la photo si une photo est sélectionnée */
if(isset($_GET["photoID"]))
{
  $photo = getPhoto($link, $_GET["photoID"] );
  $photoExploded = explode(";",$photo);
  $photoName = $photoExploded[1];
  $photoDescription = $photoExploded[2];
  $photoCategorie =getStringCat($link, $photoExploded[3]);
}
else{
  echo "<p> Aucune photo sélectionnée, affichage de la première image de la base de donnée </p>";
  $photo = getPhoto($link, 1);
  $photoExploded = explode(";",$photo);
  $photoName = $photoExploded[1];
  $photoDescription = $photoExploded[2];
  $photoCategorie =getStringCat($link, $photoExploded[3]);
}

echo "<img src='bdd/photos/$photoName' height = 200>";
echo "<table class=tableau>";
echo "<tr><td>Nom</td><td>$photoName</td></tr>";
echo "<tr><td>Description</td><td>$photoDescription</td></tr>";
echo "<tr><td>Categorie</td><td><a href = 'index.php?photo=$photoCategorie&valider=Valide'>$photoCategorie </a></td></tr>";
?>

</div>
</body>
</html>
