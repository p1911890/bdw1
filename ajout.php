<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/utilisateur.php';
require_once 'fonctions/images.php';

$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$user ="";
$stateMsg = "";

/*Cette fonction prend en paramètre une durée en secondes et la transforme en format hh/mm/ss */
function transformation($temps){
	$heures = (int) ($temps / 3600);
	$temps = $temps % 3600;
	$minutes = (int) ($temps / 60);
	$temps = $temps % 60;
	$secondes = $temps;
	
	return $heures." heures ".$minutes." minutes ".$secondes." secondes."; 
	
	
}

/*on vérifie si l'utilisateur est connecté ou non*/
if (!(isset($_SESSION["logged"])))
{
	$_SESSION["logged"] = "false";
}
else{
  if (isset ($_SESSION["user"])){
  $user = $_SESSION["user"];}
}


?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Ajouter une image</title>
  <link rel="stylesheet" href="./css/style_ajout.css">
  <link rel="icon" href="favicon.ico" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<?php 
	/*si une session est en cours, i.e. un utilisateur est connecté, on affiche le contenu de la page, qui permet d'ajouter une image*/
	if($_SESSION["logged"] == "true"){
	echo"<p>utilisateur:";
	echo$user;
	echo" actif depuis ";
	echo transformation(time() - $_SESSION["ConTime"]);
    echo"<form action='ajout.php' method='POST' enctype='multipart/form-data'>";
    echo "<h2>Ajouter un fichier</h2>";
    echo "<label for='fileUpload'>Fichier:</label>";
    echo " <input type='file' name='photo' id='fileUpload'>";    
    echo"<p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 100ko.</p>";
    echo"<div class='form-group'>";
    echo"</br>";
    echo"<label for='Description'>Description de la photo</label>";
    echo"</br>";
    echo"<label for='Description'>Les apostrophes ne peuvent pas être prises en comptes dans la base de donnée, merci de ne pas en utiliser</label>";
    echo"<textarea class='form-control' id='Description' name='Description' rows='4'></textarea>";
    echo"</div>";
    echo"</br>";
    echo"<div class='form-group'>";
    echo"<label for='Categorie'>Catégorie de la photo</label>";
    echo"<select class='form-control' name='Categorie'>";
    echo"<option>Konoha</option>";
	echo"<option>Train</option>";
	echo"<option>Chat</option>";
	echo"<option>Chien</option>";
	echo"</select>";
    echo"</div>";
    echo"</br>";
    echo" <input type='submit' name='submit' value='Upload'>";
    echo"</form>";
    echo"</div>";
} /* si l'utilisateur n'est pas connecté, il n'e peut envoyer de photos, l'ajout n'est donc pas possible*/
  else{
    echo"Vous n'êtes pas connecté";
    echo "</br>";
    echo"<a class='addPhoto' href = 'index.php'>Accueil</a>";}
  ?>

<?php
/* on traite la demande de televersion*/
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Vérifie si le fichier a été uploadé sans erreur.
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];

        // Vérifie l'extension du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.</br><a class='loginInfo' href='index.php'>Accueil</a>");

        // Vérifie la taille du fichier - 100ko maximum
        $maxsize =  1024 * 100;
        if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.</br><a class='loginInfo' href='index.php'>Accueil</a>");

        // Vérifie le type MIME du fichier
        if(in_array($filetype, $allowed)){
            // Vérifie si le fichier existe avant de le télécharger.
            if(file_exists("bdd/photos/" . $_FILES["photo"]["name"])){
                echo $_FILES["photo"]["name"] . " existe déjà.";
            } else{
				//Vérifie que la description est bien remplie
                if((strlen($_POST['Description'])>1)){
		        $fichier = "DSC".getMaxPhoto($link).getExtensionPhoto($filetype);
                move_uploaded_file($_FILES["photo"]["tmp_name"], "bdd/photos/" . $fichier);

                addPhoto($link,$_POST['Description'],$_POST['Categorie'], $fichier);
                echo "Votre fichier a été téléversé avec succès.";
                }else{
                    echo "Votre description doit faire au moins une lettre";
                }
            } 
        } else{
            echo "Error: Il y a eu un problème de téléchargement de votre fichier. Veuillez réessayer.</br>"; 
        }
    } else{
        echo "Error: Il y a eu problème durant le téléchargement de votre fichier ou vous n'avez pas déposé de fichier";
    }
}
?>
</br>
</br>
<a class="loginInfo" href="index.php">Accueil</a>
</body>
</html>

