<?php

/*Cette fonction renvoie un tableau (array) des photos. 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getAllPhoto($link)
{
	$photo = array();

	$query = "SELECT photoId, nomFich, description, catId FROM Photo;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$photo[$index] = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
		$index++;
    }
	
	return $photo;
}

/*Cette fonction renvoie un tableau (array) des categories. 
Un enregistrement est une chaine de caractères de la forme "catId;nomCat".*/
function getAllCategorie($link)
{
	$categorie = array();

	$query = "SELECT catId, nomCat FROM Categorie;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$categorie[$index] = $row["catId"].";".$row["nomCat"];
		$index++;
    }
	
	return $categorie;
}

/*Cette fonction renvoie un tableau (array) des habitants de konoha. 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getAllKonoha($link)
{
	$photo = array();

	$query = "SELECT photoId, nomFich, description, catId FROM Photo WHERE catId = 1;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$photo[$index] = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
		$index++;
    }
	
	return $photo;
}

/*Cette fonction renvoie un tableau (array) des train. 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getAllTrain($link)
{
	$photo = array();

	$query = "SELECT photoId, nomFich, description, catId FROM Photo WHERE catId = 2;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$photo[$index] = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
		$index++;
    }
	
	return $photo;
}

/*Cette fonction renvoie un tableau (array) des chat. 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getAllChat($link)
{
	$photo = array();

	$query = "SELECT photoId, nomFich, description, catId FROM Photo WHERE catId = 3;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$photo[$index] = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
		$index++;
    }
	
	return $photo;
}

/*Cette fonction renvoie un tableau (array) des photos de chien. 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getAllChien($link)
{
	$photo = array();

	$query = "SELECT photoId, nomFich, description, catId FROM Photo WHERE catId = 4;";
	$result = executeQuery($link, $query);
    
	$index = 0;
	while ($row = mysqli_fetch_assoc($result)) { //Récupère une ligne de résultat sous forme de tableau associatif
		$photo[$index] = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
		$index++;
    }
	
	return $photo;
}

/* Cette fonction renvoie une photo en fonction de son ID 
Un enregistrement est une chaine de caractères de la forme "photoId;nomFich;description;catId".*/
function getPhoto($link, $id)
{
	$query = "SELECT photoId, nomFich, description, catId FROM Photo WHERE photoID = $id;";
	$result = executeQuery($link, $query);
	$row = mysqli_fetch_assoc($result);

	$photo = $row["photoId"].";".$row["nomFich"].";".$row["description"].";".$row["catId"];
	return $photo;
}

/*Cette fonction renvoie le nom d'une catégorie dont on a mis l'id en paramètre*/
function getStringCat($link, $id)
{
	$query = "SELECT nomCat FROM Categorie WHERE catId = $id;";
	$result = executeQuery($link, $query);
	$row = mysqli_fetch_assoc($result);
	$nom = $row["nomCat"];
	return $nom;
}

/*Cette fonction ajoute une photo à la base de donnée
on ajoute le nom du fichier, une description, et l'idée de catégorie associé au nom de catégorie qu'on a choisit*/
function addPhoto($link, $desc, $cat, $nomfich){
    $query = "INSERT INTO Photo (nomFich, description, catId) VALUES ( '$nomfich', '$desc', (SELECT catId FROM Categorie WHERE nomCat LIKE '$cat'));";
    $result = executeQuery($link, $query);}

//Cette fonction renvoie la valeur max de l'ID des photos */
function getMaxPhoto($link)
{
    $query = "SELECT MAX(photoId) from Photo;";
    $result = executeQuery($link, $query);
    $row = mysqli_fetch_assoc($result);
    $max = $row["MAX(photoId)"];
    return $max+1;}

//Cette fonction renvoie l'extension du fichier */
function getExtensionPhoto($lefile){
    if($lefile == "image/jpg"){
        return ".jpg";}
    if($lefile == "image/jpeg"){
        return ".jpg";}
    if($lefile == "image/png"){
        return ".png";}
    if($lefile == "image/gif"){
        return ".gif";}
    }

?>
