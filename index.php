<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/images.php';

$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$user ="";
$stateMsg = "";

/*Cette fonction prend en paramètre une durée en secondes et la transforme en format hh/mm/ss */
function transformation($temps){
	$heures = (int) ($temps / 3600);
	$temps = $temps % 3600;
	$minutes = (int) ($temps / 60);
	$temps = $temps % 60;
	$secondes = $temps;
	
	return $heures." heures ".$minutes." minutes ".$secondes." secondes."; 
	
	
}
/*on regarde si une session est en cours, i.e. un utilisateur est connecté, puis on définit l'état de la session*/
if (!(isset($_SESSION["logged"])))
{
	$_SESSION["logged"] = "false";
}
/* Si une session est en cours, on fait en sorte de définir l'utilisateur*/
else{
  if (isset ($_SESSION["user"])){
  $user = $_SESSION["user"];}
}

/*on regarde si la déconnexion a été demandée par la méthode POST, et si c'est le cas on met fin à la session, puis on rafraichit la page*/
if(isset($_POST["Deconnexion"])){
  $_SESSION["logged"]="false";
  session_destroy();
  header('Location: index.php');
}

/*on regarde si une catégorie a été demandée, i.e. le bouton valider a été selectionné ou une catégorie est dans l'url */
if(isset($_GET["valider"]) || isset($_GET["nomCat"])){
	/*catégorie sélectionnée: toutes les photos*/
  if($_GET['photo']=='toutes'){
    $recordsP = getAllPhoto($link);
    $titre= "Toutes les photos";
  }
	/*catégorie sélectionnée : photos de konoha*/
  if($_GET['photo']=='Konoha'){
    $recordsP = getAllKonoha($link);
    $titre= "Les photos de la catégorie Konoha";
  }
	/*catégorie sélectionnée : photos de trains*/
  if($_GET['photo']=='Train'){
    $recordsP = getAllTrain($link);
    $titre= "Les photos de la catégorie Train";
  }
	/*catégorie sélectionnée : photos de chats*/
  if($_GET['photo']=='Chat'){
    $recordsP = getAllChat($link);
    $titre= "Les photos de la catégorie Chat";
  }
	/*catégorie sélectionnée : photos de chiens*/
  if($_GET['photo']=='Chien'){
    $recordsP = getAllChien($link);
    $titre= "Les photos de la catégorie Chien";
  }
}
/*Dans le cas où aucune demande n'a été faite, on affiche toutes les photos*/
  else{
    $recordsP = getAllPhoto($link);
    $titre= "Toutes les photos";
}

?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mini Pinterest</title>
  <link rel="stylesheet" href="./css/style.css">
  <link rel="icon" href="favicon.ico" />
</head>
<body>
  <?php 
	/*si une session est en cours, i.e. un utilisateur est connecté, on affiche l'identifiant, le temps de connexion, le bouton de déconnexion et d'ajout d'image*/
	if($_SESSION["logged"] == "true"){
	echo"<p>utilisateur:";
	echo$user;
	echo" actif depuis ";
	echo transformation(time() - $_SESSION["ConTime"]);
    echo"<form action='index.php' method = 'POST'>";
    echo"<input class='button' type='submit' name='Deconnexion' value='disconnect'>";
    echo"</form>";
    echo"</br>";
    echo"</br>";
    echo"<a class='addPhoto' href = 'ajout.php'>Ajouter une photo</a>";
    echo"</br>";
    echo"</br>";}
    /*si aucune session n'est en cours, on affiche les boutons d'inscription et de connexion*/
  else{
    echo"</br>";
    echo"<a class='loginInfo' href ='connexion.php'>Se connecter</a>";// ici on démarre l'action connexion.php au clic
    echo"</br>";
    echo"</br>";
    echo"</br>";
    echo"<a class='loginInfo' href='inscription.php'>S'inscrire</a>";// ici on démarre l'application inscription.php au clic
    echo"</br>";
    echo"</br>";}
  ?>

  <?php 
    $tabPhoto =  '<table>'; 
    $nbPhoto = sizeof($recordsP) - 1;
    $i = $nbPhoto;
    $nbPhotoAff = $nbPhoto + 1;

    $recordsC = getAllCategorie($link);
    $nbCategorie = sizeof($recordsC) - 1;
    $k = $nbCategorie;

    echo "<div class=SelectPhoto>";

    echo "<p>$nbPhotoAff photo(s) selectionnée(s)</p>";

    echo "</div>";

  ?>
  
  

  <div class=ChoixCat>

  <br />

  <p>Quelles photos souhaitez-vous afficher?</p>

  <form action='index.php' method='GET'>

  <select name='photo' id='photo-select'>

  <option value='toutes'>Toutes les photos</option>

  <?php

    for($k;$k>=0;$k--){

      $recC = $recordsC[$k];
      $splitRecC = explode(";", $recC);
      $nomCat = $splitRecC[1];
      echo "<option value=$nomCat>$nomCat</option>"; // on affiche les catégories dans un menu déroulant

    }

  ?>
    
  </select>

  <input class='button' type='submit' name='valider' value='Valider'>

  </form>

  <?php

    echo "<p><font size=6>$titre</p>";

    echo "</div>";

  
    /* on affiche les images de la plus récente (id le plus élevé) à la plus ancienne*/            
    while($i >= 0){

      $tabPhoto .= '<tr>';

      for($j=0; $j<=6; $j++){
        if($i >=0){
            $recP = $recordsP[$i];
            $splitRecP = explode(";", $recP);
            $photoID = $splitRecP[0];
            $nomFich = $splitRecP[1];
            $tabPhoto .= '<td><a href="description.php?photoID='.$photoID.'"><input type="image" src="bdd/photos/'.$nomFich.'" height="200" width="200" ></a></td>';
            $i--;}
      }

      $tabPhoto .= '</tr>';

    }

    $tabPhoto .='</table>';
    echo $tabPhoto;
  ?>

</body>
</html>
