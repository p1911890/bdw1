-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 07 mai 2021 à 02:52
-- Version du serveur :  10.3.25-MariaDB-0ubuntu0.20.04.1-log
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `p1911890`
--

-- --------------------------------------------------------

--
-- Structure de la table `Categorie`
--

CREATE TABLE `Categorie` (
  `catId` int(11) NOT NULL,
  `nomCat` varchar(250) NOT NULL,
   PRIMARY KEY (catId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Categorie`
--

INSERT INTO `Categorie` (`catId`, `nomCat`) VALUES
(1, 'Konoha'),
(2, 'Train'),
(3, 'Chat'),
(4, 'Chien');

-- --------------------------------------------------------

--
-- Structure de la table `Photo`
--

CREATE TABLE IF NOT EXISTS Photo (
  photoId integer NOT NULL AUTO_INCREMENT,
  nomFich varchar(250) NOT NULL,
  description varchar(250) NOT NULL,
  catId integer NOT NULL ,
  PRIMARY KEY (photoId),
  FOREIGN KEY (catId) REFERENCES Categorie(catId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Photo`
--

INSERT INTO `Photo` (`nomFich`, `description`, `catId`) VALUES
('bebechat.jpg', 'Un chaton super mignon', 3),
('chatroux.jpg', 'Un chat roux qui est très beau', 3),
('chatsombre.jpg', 'Un chat blanc et noir pour plus de diversités', 3),
('chiendrole.jpg', 'Un chien qui a une tête très rigolote', 4),
('chienloup.jpg', 'Un chien loup aussi appelé husky', 4),
('itachi.jpg', 'Itachi le traître qui a tué toute la famille c\'est pas cool', 1),
('jiraya.jpg', 'RIP', 1),
('naruto.jpg', 'Il est tout seul sauf qu\'il est devenu copain avec Sasuke et un plot et depuis tout va mieux', 1),
('rer.jpg', 'Le rer', 2),
('sasuke.jpg', 'L\'être humain le plus sombre de l\'univers', 1),
('ter.jpg', 'Le ter', 2),
('tgv.jpg', 'Le tgv', 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `pseudo` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (pseudo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
